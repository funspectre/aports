# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.2.4
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/sumneko/lua-language-server"
arch="all !s390x !ppc64le" # ftbfs
license="MIT"
makedepends="bash samurai"
source="https://github.com/sumneko/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/sumneko/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	default_prepare

	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
41b6bbe0c7145ac692bae957467241f6547fcc2c5990e4bf0b2d2ca8d02024b9fbb671c6354a3a9bf3d60417c2014d2a625b2b50f2ca39192b95a16489c51136  lua-language-server-3.2.4.tar.gz
a1eb68bed85af3b49a29d7d6135aa7e2551bc4847328e8cf1322bcce230d9b5099894ee69cd4b76bdc2bd38e98c2e71244d613191fb5e570ead6310182b76e96  lua-language-server-submodules-3.2.4.zip.noauto
9fa9621b61a365a576079731afe419245268b5223292989d2f98091a26b8866ba97a8c6c4cf8e5cbb2704089cb45167630557049430105a71ed4fd55311a543a  wrapper
"
