# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=rage
pkgver=0.8.0
pkgrel=1
pkgdesc="Simple, modern and secure encryption tool"
url="https://github.com/str4d/rage"
license="Apache-2.0 OR MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
makedepends="cargo fuse-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/str4d/rage/archive/v$pkgver/rage-$pkgver.tar.gz
	fix-tests.patch
	"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --features mount

	cargo run --frozen --release --example generate-docs
	cargo run --frozen --release --example generate-completions
}

check() {
	cargo test --frozen --features mount
}

package() {
	for cmd in rage rage-keygen rage-mount; do
		install -Dm755 target/release/$cmd -t "$pkgdir"/usr/bin

		install -Dm644 target/manpages/$cmd.1.gz -t "$pkgdir"/usr/share/man/man1

		install -Dm644 target/completions/$cmd.bash \
			"$pkgdir"/usr/share/bash-completion/completions/$cmd
		install -Dm644 target/completions/$cmd.fish \
			"$pkgdir"/usr/share/fish/completions/$cmd.fish
		install -Dm644 target/completions/$cmd.zsh \
			"$pkgdir"/usr/share/zsh/site-functions/_$cmd
	done
}

sha512sums="
bbe304ac82bec4d46b7f00ecb6dd320827479b2901e8465c155871f5cf5a4af3445d51c4dc79496855ed746e5bc3b31816f4654a95dcc085da3f1b3246dfafb8  rage-0.8.0.tar.gz
a746504996c3327063ffe6e2c02421811b94787c4e3276a7cdeb0c4a8c05c7530d5988b8ac963f4d8d60be7ed3bca91e76c0239f6dff7b0dc0f441bfb258b3e0  fix-tests.patch
"
