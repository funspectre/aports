# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkexiv2
pkgver=22.04.1
pkgrel=0
pkgdesc="A library to manipulate pictures metadata"
url="https://www.kde.org/applications/graphics"
arch="all !armhf" # extra-cmake-modules
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev exiv2-dev samurai"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
5e227db57c973411f63bc0296d29439929ec2df2a25e93d7eef42921370a31b3e8aa6c9e3cc04fe1f1690f62995563143c83c7752460a3fc9981ecb9d7038ae2  libkexiv2-22.04.1.tar.xz
"
