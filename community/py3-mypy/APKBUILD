# Contributor: Justin Berthault <justin.berthault@zaclys.net>
# Maintainer: Justin Berthault <justin.berthault@zaclys.net>
pkgname=py3-mypy
pkgver=0.960
pkgrel=0
pkgdesc="Optional static typing for Python (PEP484)"
options="!check" # Tests fail on builders, pass on CI
url="https://www.mypy-lang.org/"
arch="noarch"
license="MIT"
depends="
	py3-mypy-extensions
	py3-tomli
	py3-typing-extensions
	"
makedepends="python3-dev py3-setuptools"
checkdepends="py3-pytest py3-pytest-xdist py3-lxml py3-virtualenv"
source="https://files.pythonhosted.org/packages/source/m/mypy/mypy-$pkgver.tar.gz"
builddir="$srcdir/"mypy-$pkgver

build() {
	python3 setup.py build
}

check() {
	rm -f \
		mypyc/test/test_analysis.py \
		mypyc/test/test_exceptions.py \
		mypyc/test/test_refcount.py \
		mypyc/test/test_run.py
	py.test-3 -v
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
10b754f3d85e86f1362ff6f9511f941b0ae3bd944d58a1340157e36d1792b6dc175e4cc85aa53b054a73d9fc8c48fe9ba026be917d01c4020612ab652205cd7d  mypy-0.960.tar.gz
"
