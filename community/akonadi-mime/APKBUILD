# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-mime
pkgver=22.04.1
pkgrel=0
pkgdesc="Libraries and daemons to implement basic email handling"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev>=$pkgver
	kcodecs-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kmime-dev
	kxmlgui-dev
	libxslt-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# mailserializerplugintest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "mailserializerplugintest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
da64a47701fe7eb0e00ce3b62c46f4a66643e78dbe01cf0a3381984813400b8b851f7880335b94e5153bba05a94d44fb110e8735e72c9a0def2fa80741750a2b  akonadi-mime-22.04.1.tar.xz
"
