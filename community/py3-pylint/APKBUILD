# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pylint
pkgver=2.14.0
pkgrel=0
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
url="https://github.com/PyCQA/pylint"
arch="noarch !s390x" # py3-dill
license="GPL-2.0-or-later"
depends="
	py3-astroid
	py3-dill
	py3-isort
	py3-mccabe
	py3-platformdirs
	py3-tomli
	py3-tomlkit
	"
makedepends="py3-setuptools"
checkdepends="
	py3-pytest
	py3-pytest-runner
	"
options="!check" # https://github.com/PyCQA/pylint/issues/3895
source="https://files.pythonhosted.org/packages/source/p/pylint/pylint-$pkgver.tar.gz
	"
builddir="$srcdir"/pylint-$pkgver

build() {
	python3 setup.py build
}

check() {
	# Requires unpackaged 'pytest-benchmark'
	rm -rf tests/benchmark
#	PYTHONPATH="$PWD/build/lib" py.test-3
	python3 setup.py pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
72a0f4d725d931c139dad23e7623d6d85c78eef70daac52334d7ad33a365e51ac83db11e464386b30ec4db8ad744324d0f048a158f1fff1a323e4c7d022cbc72  pylint-2.14.0.tar.gz
"
