# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-notes
pkgver=22.04.1
pkgrel=0
pkgdesc="Libraries and daemons to implement management of notes"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	ki18n-dev
	kmime-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-notes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7229a7c790c75f5f5b7d022ce93a871b54555435b0711db6bde39d366cd80670487b189d92eb266d8cbceea1abc7a9835763f9cf6d0fb699bbbacd838f48d99c  akonadi-notes-22.04.1.tar.xz
"
